package com.alhali.corp.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;

import com.alhali.corp.models.CreateProductCallbackRequest;
import com.alhali.corp.models.CreateProductCallbackResponse;
import com.alhali.corp.processors.ExceptionProcessor;
import com.alhali.corp.processors.RequestProcessor;
import com.alhali.corp.processors.ResponseProcessor;

@org.springframework.stereotype.Component
public class ProductCallbackRoutes extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		
		restConfiguration()
		.component("undertow").host("0.0.0.0").port("8081").bindingMode(RestBindingMode.auto).scheme("http")
		.dataFormatProperty("prettyPrint", "true")
		.enableCORS(true);
		
		rest("/api/v1")
		
	    .post("product-callback/")
	    .type(CreateProductCallbackRequest.class)
		.consumes("application/json").produces("application/json")
		.to("direct: invoke-product-callback").outType(CreateProductCallbackResponse.class)
		
		.get("healthz")
		.to("direct: healthz");
		
		from("direct: healthz")
		.removeHeader("CamelHttp*")
		.log("Incoming!")
		.setBody().constant("Running...");
		
		 from("direct: invoke-product-callback")
			.removeHeader("CamelHttp*")
		 	.process(new RequestProcessor())
		 	.setHeader(CxfConstants.OPERATION_NAME, constant("CreateProductCallback"))
			.setHeader(CxfConstants.OPERATION_NAMESPACE, constant("http://corp.alahli.com/middlewareservices/outbounddialer/1.0/"))
			.log("incomig1")
			.to("cxf:bean:cxfCustomerContact")
			.log("Incoming-1")
			.process(new ResponseProcessor())
			.unmarshal().json(JsonLibrary.Jackson , CreateProductCallbackResponse.class)
			.onException(java.lang.Exception.class)
		    	.process(new ExceptionProcessor())
		    .end();
	}

}
