package com.alhali.corp.models;

public class CustomException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FaultType fault;
	public String status;

	public CustomException(FaultType fault, String status) {
		super(status);
		this.fault = fault;
	}
	public FaultType getFault() {
		return fault;
	}
	public void setFault(FaultType fault) {
		this.fault = fault;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	


	
	
	
}
