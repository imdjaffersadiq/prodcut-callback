package com.alhali.corp.models;

import java.io.Serializable;

public class CreateProductCallbackResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected CreateProductResponseDetails success;
	protected FaultType fault;
	
	
	public CreateProductResponseDetails getSuccess() {
		return success;
	}
	public void setSuccess(CreateProductResponseDetails success) {
		this.success = success;
	}

	
	public FaultType getFault() {
		return fault;
	}
	public void setFault(FaultType fault) {
		this.fault = fault;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

	
}
