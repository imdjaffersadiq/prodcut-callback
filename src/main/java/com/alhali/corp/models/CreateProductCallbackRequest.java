package com.alhali.corp.models;

import java.io.Serializable;

public class CreateProductCallbackRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    protected String cif;
    protected String phonenumber;
    protected String productType;
    protected String productCode;
    protected String attribute1;
    protected String attribute2;
    protected String attribute3;
    protected String attribute4;
    protected String attribute5;
    protected String attribute6;
    protected String attribute7;
    protected String attribute8;
    protected String attribute9;
    protected String attribute10;
    protected String attribute11;
    protected String attribute12;
    protected String attribute13;
    protected String attribute14;
    protected String attribute15;
	public String getCif() {
		return cif;
	}
	public void setCif(String cif) {
		this.cif = cif;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getAttribute1() {
		return attribute1;
	}
	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}
	public String getAttribute2() {
		return attribute2;
	}
	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}
	public String getAttribute3() {
		return attribute3;
	}
	public void setAttribute3(String attribute3) {
		this.attribute3 = attribute3;
	}
	public String getAttribute4() {
		return attribute4;
	}
	public void setAttribute4(String attribute4) {
		this.attribute4 = attribute4;
	}
	public String getAttribute5() {
		return attribute5;
	}
	public void setAttribute5(String attribute5) {
		this.attribute5 = attribute5;
	}
	public String getAttribute6() {
		return attribute6;
	}
	public void setAttribute6(String attribute6) {
		this.attribute6 = attribute6;
	}
	public String getAttribute7() {
		return attribute7;
	}
	public void setAttribute7(String attribute7) {
		this.attribute7 = attribute7;
	}
	public String getAttribute8() {
		return attribute8;
	}
	public void setAttribute8(String attribute8) {
		this.attribute8 = attribute8;
	}
	public String getAttribute9() {
		return attribute9;
	}
	public void setAttribute9(String attribute9) {
		this.attribute9 = attribute9;
	}
	public String getAttribute10() {
		return attribute10;
	}
	public void setAttribute10(String attribute10) {
		this.attribute10 = attribute10;
	}
	public String getAttribute11() {
		return attribute11;
	}
	public void setAttribute11(String attribute11) {
		this.attribute11 = attribute11;
	}
	public String getAttribute12() {
		return attribute12;
	}
	public void setAttribute12(String attribute12) {
		this.attribute12 = attribute12;
	}
	public String getAttribute13() {
		return attribute13;
	}
	public void setAttribute13(String attribute13) {
		this.attribute13 = attribute13;
	}
	public String getAttribute14() {
		return attribute14;
	}
	public void setAttribute14(String attribute14) {
		this.attribute14 = attribute14;
	}
	public String getAttribute15() {
		return attribute15;
	}
	public void setAttribute15(String attribute15) {
		this.attribute15 = attribute15;
	}    
    
    
}
