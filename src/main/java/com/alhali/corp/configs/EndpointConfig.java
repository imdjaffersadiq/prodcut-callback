package com.alhali.corp.configs;


import java.util.HashMap;
import java.util.Map;

import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.transport.http.auth.HttpAuthHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alahli.corp.middlewareservices.outbounddialer._1_0.CustomerContact;


@Configuration
public class EndpointConfig {

	static Logger log =LoggerFactory.getLogger(EndpointConfig.class);
	
	@Value("${endpoint.wsdl}")
	private String SOAP_URL; //WSDL URL
	
	@Value("${endpoint.username}")
	private String USERNAME;
	
	@Value("${endpoint.password")
	private String PASSWORD;


    @Bean(name="cxfCustomerContact")
	public CxfEndpoint buildCxfEndpoint() {
        log.info("Target SOAP_URL: " + SOAP_URL);
        log.info("Target Username: " + USERNAME);
        log.info("Target Password: ********");

		CxfEndpoint endpoint = new CxfEndpoint();
		endpoint.setAddress(SOAP_URL);
		endpoint.setServiceClass(CustomerContact.class);
		
		return attachProperties(endpoint);


    }


	private CxfEndpoint attachProperties(CxfEndpoint endpoint) {
		
		Map<String, Object> properties = new HashMap<String, Object>(); 

		AuthorizationPolicy authPolicy = new AuthorizationPolicy(); 
		authPolicy.setAuthorizationType(HttpAuthHeader.AUTH_TYPE_BASIC); 
		authPolicy.setUserName(USERNAME);
		authPolicy.setPassword(PASSWORD); 
		authPolicy.setAuthorization("true");
		properties.put("org.apache.cxf.configuration.security.AuthorizationPolicy", authPolicy);
		endpoint.setProperties(properties);

		return endpoint;
	}
	
	
	
	
	
}
