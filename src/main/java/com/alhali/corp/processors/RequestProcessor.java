package com.alhali.corp.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alhali.corp.models.CreateProductCallbackRequest;
import com.alhali.corp.models.CustomException;
import com.alhali.corp.models.FaultType;
import com.alhali.corp.models.HeadersRequest;
import com.alhali.corp.utils.Utils;

public class RequestProcessor implements Processor {

	static Logger log = LoggerFactory.getLogger(RequestProcessor.class);

	@Override
	public void process(Exchange ex) throws Exception {
		log.debug("RequestProcessor.process");
		Utils utils = new Utils();

		Message message = ex.getIn();
		log.debug("Request message type : " + message);

		HeadersRequest requestHeaders = utils.getDefaultRequestHeaders(message);

		CreateProductCallbackRequest request = message.getBody(CreateProductCallbackRequest.class);
		
		if ( request == null){
			FaultType fault = getFault("Client Error", "Null or Empty Request");
			log.debug("Fault Message: "+ fault.getDescription());
			throw new CustomException(fault, "Error");
		}
		log.info("Request: " + utils.getJsonBody(request));
		log.info("CIF: " + request.getCif());
		
		com.alahli.corp.middlewareservices.outbounddialer._1.CreateProductCallbackRequest requestPayload = createTargetCreateProductCallbackRequest(request);
		log.info("Request (Cordys): "+ utils.getXmlBody(requestPayload));
		
	//	ex.getIn().setHeader("Accept-Encoding", "*");
		//ex.getIn().setBody(requestPayload);
		ex.getIn().setBody(requestPayload);
	}

	public com.alahli.corp.middlewareservices.outbounddialer._1.CreateProductCallbackRequest createTargetCreateProductCallbackRequest(CreateProductCallbackRequest	request) {
		log.info("inside the class");
		log.debug("RequestProcessor.createTargetCreateProductCallbackRequest");
	
		Utils utils = new Utils();

		com.alahli.corp.middlewareservices.outbounddialer._1.CreateProductCallbackRequest targetRequest = new com.alahli.corp.middlewareservices.outbounddialer._1.CreateProductCallbackRequest();

		targetRequest.setCif(utils.checkNull(request.getCif()));
		targetRequest.setPhoneNumber(utils.checkNull(request.getPhonenumber()));
		targetRequest.setProductCode(utils.checkNull(request.getProductCode()));
		targetRequest.setProductType(utils.checkNull(request.getProductType()));
		targetRequest.setAttribute1(utils.checkNull(request.getAttribute1()));
		targetRequest.setAttribute2(utils.checkNull(request.getAttribute2()));
		targetRequest.setAttribute3(utils.checkNull(request.getAttribute3()));
		targetRequest.setAttribute4(utils.checkNull(request.getAttribute4()));
		targetRequest.setAttribute5(utils.checkNull(request.getAttribute5()));
		targetRequest.setAttribute6(utils.checkNull(request.getAttribute6()));
		targetRequest.setAttribute7(utils.checkNull(request.getAttribute7()));
		targetRequest.setAttribute8(utils.checkNull(request.getAttribute8()));
		targetRequest.setAttribute9(utils.checkNull(request.getAttribute9()));
		targetRequest.setAttribute10(utils.checkNull(request.getAttribute10()));
		targetRequest.setAttribute11(utils.checkNull(request.getAttribute11()));
		targetRequest.setAttribute12(utils.checkNull(request.getAttribute12()));
		targetRequest.setAttribute13(utils.checkNull(request.getAttribute13()));
		targetRequest.setAttribute14(utils.checkNull(request.getAttribute14()));
		targetRequest.setAttribute15(utils.checkNull(request.getAttribute15()));
		
		
		return targetRequest;
	}
	
	public FaultType getFault(String message, String description) {
		FaultType fault = new FaultType();
		
		fault.setNativeError(message);
		fault.setDescription(description);
		return fault;
	}

}
