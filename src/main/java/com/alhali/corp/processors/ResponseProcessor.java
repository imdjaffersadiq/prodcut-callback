package com.alhali.corp.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.cxf.message.MessageContentsList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alahli.corp.middlewareservices.outbounddialer._1.CreateProductCallbackResponse;
import com.alhali.corp.models.CreateProductResponseDetails;
import com.alhali.corp.models.CustomException;
import com.alhali.corp.models.FaultType;
import com.alhali.corp.utils.Utils;

public class ResponseProcessor implements Processor {
	static Logger log =LoggerFactory.getLogger(ResponseProcessor.class);

	@Override
	public void process(Exchange ex) throws Exception {
		log.debug("ResponseProcessor.process");
		Utils utils = new Utils();

		MessageContentsList targetResponse = ex.getIn().getBody(MessageContentsList.class);
		
		if (targetResponse == null || targetResponse.size()<1) {
			
		}
		CreateProductCallbackResponse productCallbackResponse = (CreateProductCallbackResponse) targetResponse.get(0);
	

		log.info("Response (Cordys): " +utils.getXmlBody(productCallbackResponse));
		log.info("Resposne Reference Number: "+ productCallbackResponse.getSuccess().getReferenceNumber());
		
		com.alhali.corp.models.CreateProductCallbackResponse resp = new com.alhali.corp.models.CreateProductCallbackResponse();
		
		CreateProductResponseDetails respType = new CreateProductResponseDetails();
		respType.setReferenceNumber(productCallbackResponse.getSuccess().getReferenceNumber());
		respType.setStatus(productCallbackResponse.getSuccess().getStatus());

		
		FaultType fault = new FaultType();
		
		resp.setSuccess(respType);
		resp.setFault(fault);
		
		ex.getIn().setHeaders(utils.getDefaultResponseHeaders());
		String jsonResp = utils.getJsonBody(resp);
		
		log.info("Response: "+jsonResp);
		ex.getIn().setBody(jsonResp);
	}
}
