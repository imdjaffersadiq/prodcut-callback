package com.alhali.corp.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alhali.corp.models.CustomException;
import com.alhali.corp.models.FaultType;
import com.alhali.corp.utils.Utils;

public class ExceptionProcessor implements Processor {
	static Logger log =LoggerFactory.getLogger(ExceptionProcessor.class);

	@Override
	public void process(Exchange ex) throws Exception {
		log.debug("ExceptionProcessor.process");
		Utils utils = new Utils();
		CustomException excep = ex.getIn().getBody(CustomException.class);
		FaultType fault = new FaultType();
		
		//CreateProductCAllbackResponse respType = new CustomerInquiryResponseType();
		com.alhali.corp.models.CreateProductCallbackResponse resp = new com.alhali.corp.models.CreateProductCallbackResponse();
		resp.setFault(excep.getFault());

		ex.getIn().setHeaders(utils.getDefaultResponseHeaders());
		String jsonResp = utils.getJsonBody(resp);

		log.info("Response: " + jsonResp);
		ex.getIn().setBody(jsonResp);

	}

}
