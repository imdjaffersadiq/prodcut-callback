package com.alhali.corp.utils;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXB;

import org.apache.camel.Message;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;


import com.alhali.corp.models.HeadersRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Utils {

	public HeadersRequest getDefaultRequestHeaders(Message message) {

		HeadersRequest requestHeaders = new HeadersRequest();
		String loggerContext = message.getHeader("loggerContext", String.class);
		String transactionCode = message.getHeader("transactionCode", String.class);
		String retryTransaction = message.getHeader("retryTransaction", String.class);
		String tellerId = message.getHeader("tellerId", String.class);
		String terminalId = message.getHeader("terminalId", String.class);
		String workstationId = message.getHeader("workstationId", String.class);
		String override = message.getHeader("override", String.class);
		String correction = message.getHeader("correction", String.class);
		String supervisor = message.getHeader("supervisor", String.class);
		String supervisorId = message.getHeader("supervisorId", String.class);
		String overrideCode = message.getHeader("overrideCode", String.class);
		String employeeId = message.getHeader("employeeId", String.class);
		String branchId = message.getHeader("branchId", String.class);
		String functionId = message.getHeader("functionId", String.class);
		String channelId = message.getHeader("channelId", String.class);
		String bancsPwd = message.getHeader("bancsPwd", String.class);
		String languageCode = message.getHeader("languageCode", String.class);
		String authenticationType = message.getHeader("authenticationType", String.class);
		String IvrTransactionID = message.getHeader("IvrTransactionID", String.class);

		requestHeaders.setLoggerContext(checkNull(loggerContext));
		requestHeaders.setTransactionCode(checkNull(transactionCode));
		// TODO: Set other request headers
		return requestHeaders;
	}

	public String checkNull(String field) {
		if (field == null) {
			return "";
		}
		return field;
	}

	public Map<String, Object> getDefaultResponseHeaders() {
		Map<String, Object> headersMap = new HashMap<String, Object>();
		headersMap.put("Content-Type", "application/json");
		headersMap.put("Signature", "Farzam Alam");
		return headersMap;

	}

	public String getJsonBody(Object resp) {
		ObjectMapper ow = new ObjectMapper();
		try {
			String json = ow.writerWithDefaultPrettyPrinter().writeValueAsString(resp);
			return json;
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public String getXmlBody(Object obj) {
		StringWriter sw = new StringWriter();
		JAXB.marshal(obj, sw);
		return sw.toString();
	}
}
