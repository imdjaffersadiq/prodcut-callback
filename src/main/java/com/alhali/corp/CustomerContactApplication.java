package com.alhali.corp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerContactApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerContactApplication.class, args);
	}

}
